from __future__ import print_function

# Log imports
import logging
import logging.config

# GRPC imports
import grpc
from generated import activities_sniffer_service_pb2_grpc as as_service_grpc
from generated import activities_sniffer_service_pb2 as as_service

import datetime
import os.path

from googleapiclient.discovery import build
from google_auth_oauthlib.flow import InstalledAppFlow
from google.auth.transport.requests import Request
from google.oauth2.credentials import Credentials

import json

SCOPES = ['https://www.googleapis.com/auth/calendar']
ACTYVITY_SNIFFER_SERVICE_ADDRESS = 'localhost:50051'
EXTRANET_ADDRESS = 'https://extranet-clubalpin.com/app/out/out.php?s=12&c=6430&h=0a23761ce2'
STANISLAS_CREDS = 'stanislasRazerCredentials.json'

def firstTest(service):
    """
    First personal test function.
    Get the 10 incoming events and print them.
    """
    now = datetime.datetime.utcnow().isoformat() + 'Z'  # 'Z' indicates UTC time

    calendarLogger.info('Getting the upcoming 10 events')
    events_result = service.events().list(calendarId='primary', timeMin=now,
                                          maxResults=10, singleEvents=True,
                                          orderBy='startTime').execute()

    events = events_result.get('items', [])

    if not events:
        calendarLogger.data('No upcoming events found.')

    for event in events:
        start = event['start'].get('dateTime', event['start'].get('date'))
        calendarLogger.data(event['summary'])

def firstInitCalendars(service, dellAll=0):
    """
    Create all necessary calendars regarding a json file.
    Take into account if the calendar already exist or not.
    """

    calendarLogger.info("Requesting calendars list")
    event_result = service.calendarList().list(minAccessRole="owner").execute()

    calendar_list_summary = []

    for calendar in event_result['items']:
        calendar_list_summary.append(calendar.get('summary'))

    calendarLogger.data(f"Existing calendar list Summary: {calendar_list_summary}")

    file = open('./json/calendarSetup.json')
    calendars = json.load(file)

    for calendar in calendars['calendars']:
        if calendar.get('summary') not in calendar_list_summary:
            created_calendar = service.calendars().insert(body=calendar).execute()
            calendarLogger.info(f"Calendar {created_calendar.get('id')} created ({created_calendar.get('summary')})")
        else:
            calendarLogger.info(f"Calendar {calendar.get('summary')} already in the list")


def createToken(creds):
    """
    Create a token connexion.
    Return the credential.
    """

    calendarLogger.info("Start create the Token.")

    flow = InstalledAppFlow.from_client_secrets_file(STANISLAS_CREDS, SCOPES)
    creds = flow.run_local_server(port=0)
    # Save the credentials for the next run
    with open('token.json', 'w') as token:
        token.write(creds.to_json())

    calendarLogger.info("Credentials ready !")
    return creds

def getActivities():
    """
    Open a GRPC connexion with the AcititySnifferService
    Make a GRPC request to get all activities elements.
    Returne the elements.
    """
    with grpc.insecure_channel(ACTYVITY_SNIFFER_SERVICE_ADDRESS) as channel:
        stub = as_service_grpc.ActivitiesSniffer_ServiceStub(channel)
        calendarLogger.info("Connexion with the activity sniffer service Open.")
        calendarLogger.info("Requesting activities.")
        calendarLogger.info("Waiting response.")

        responses = stub.GetActivities(as_service.GetActivities_Request(page_address=EXTRANET_ADDRESS))

        calendarLogger.info("Response has been received.")
        return responses

def main():
    """
    Main function
    """
    creds = None

    # Check if there is a token
    if(os.path.exists('token.json')):
        creds = Credentials.from_authorized_user_file('token.json', SCOPES)
        creds.refresh(Request())
        calendarLogger.info("Credentials ready !")

    else:
        calendarLogger.info("No token available.")
        creds = createToken(creds)

    # Create the API sevice access
    service = build('calendar', 'v3', credentials=creds)
    firstInitCalendars(service)

    #activities = getActivities()
    #print(activities)


if __name__ == '__main__':
    """ Main function that setup the service """

    # create console handler with a higher log level
    DATA_LEVELV_NUM = 15
    logging.addLevelName(DATA_LEVELV_NUM, "DATA")

    def data(self, message, *args, **kws):
        if self.isEnabledFor(DATA_LEVELV_NUM):
            # Yes, logger takes its '*args' as 'args'.
            self._log(DATA_LEVELV_NUM, message, args, **kws)

    # Update the config path and crete the snifferService logger
    logging.config.fileConfig('logging.conf')
    logging.Logger.data = data

    calendarLogger = logging.getLogger('calendarService')
    calendarLogger.info("Calendar Service init Ok")

    main()
