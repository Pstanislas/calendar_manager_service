# -*- coding: utf-8 -*-
# Generated by the protocol buffer compiler.  DO NOT EDIT!
# source: activities_sniffer_service.proto
"""Generated protocol buffer code."""
from google.protobuf import descriptor as _descriptor
from google.protobuf import message as _message
from google.protobuf import reflection as _reflection
from google.protobuf import symbol_database as _symbol_database
# @@protoc_insertion_point(imports)

_sym_db = _symbol_database.Default()




DESCRIPTOR = _descriptor.FileDescriptor(
  name='activities_sniffer_service.proto',
  package='activitiessniffer',
  syntax='proto3',
  serialized_options=None,
  create_key=_descriptor._internal_create_key,
  serialized_pb=b'\n activities_sniffer_service.proto\x12\x11\x61\x63tivitiessniffer\"V\n\x04\x44\x61te\x12\x15\n\rdateBeginning\x18\x01 \x01(\t\x12\x0f\n\x07\x64\x61teEnd\x18\x02 \x01(\t\x12\x15\n\rhourBeginning\x18\x03 \x01(\t\x12\x0f\n\x07hourEnd\x18\x04 \x01(\t\"D\n\nDifficulty\x12\x10\n\x08Physical\x18\x01 \x01(\x05\x12\x11\n\tTechnical\x18\x02 \x01(\x05\x12\x11\n\tElevation\x18\x03 \x01(\x05\"Q\n\x0cRegistration\x12\r\n\x05State\x18\x01 \x01(\t\x12\r\n\x05NbMax\x18\x02 \x01(\x05\x12\x0c\n\x04NbOk\x18\x03 \x01(\x05\x12\x15\n\rNbWaitingList\x18\x04 \x01(\x05\"@\n\x0b\x44\x65scription\x12\x0f\n\x07General\x18\x01 \x01(\t\x12\r\n\x05Level\x18\x02 \x01(\t\x12\x11\n\tEquipment\x18\x03 \x01(\t\"\xad\x02\n\x08\x41\x63tivity\x12\n\n\x02ID\x18\x01 \x01(\t\x12\r\n\x05Title\x18\x02 \x01(\t\x12)\n\x08\x44\x61teInfo\x18\x03 \x01(\x0b\x32\x17.activitiessniffer.Date\x12\r\n\x05Place\x18\x04 \x01(\t\x12\x35\n\x0e\x44ifficultyInfo\x18\x05 \x01(\x0b\x32\x1d.activitiessniffer.Difficulty\x12\x39\n\x10RegistrationInfo\x18\x06 \x01(\x0b\x32\x1f.activitiessniffer.Registration\x12\x13\n\x0bResponsible\x18\x07 \x01(\t\x12\x37\n\x0f\x44\x65scriptionInfo\x18\x08 \x01(\x0b\x32\x1e.activitiessniffer.Description\x12\x0c\n\x04Link\x18\t \x01(\t\"-\n\x15GetActivities_Request\x12\x14\n\x0cpage_address\x18\x01 \x01(\t\"I\n\x16GetActivities_Response\x12/\n\nActivities\x18\x01 \x03(\x0b\x32\x1b.activitiessniffer.Activity2\x81\x01\n\x19\x41\x63tivitiesSniffer_Service\x12\x64\n\rGetActivities\x12(.activitiessniffer.GetActivities_Request\x1a).activitiessniffer.GetActivities_Responseb\x06proto3'
)




_DATE = _descriptor.Descriptor(
  name='Date',
  full_name='activitiessniffer.Date',
  filename=None,
  file=DESCRIPTOR,
  containing_type=None,
  create_key=_descriptor._internal_create_key,
  fields=[
    _descriptor.FieldDescriptor(
      name='dateBeginning', full_name='activitiessniffer.Date.dateBeginning', index=0,
      number=1, type=9, cpp_type=9, label=1,
      has_default_value=False, default_value=b"".decode('utf-8'),
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR,  create_key=_descriptor._internal_create_key),
    _descriptor.FieldDescriptor(
      name='dateEnd', full_name='activitiessniffer.Date.dateEnd', index=1,
      number=2, type=9, cpp_type=9, label=1,
      has_default_value=False, default_value=b"".decode('utf-8'),
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR,  create_key=_descriptor._internal_create_key),
    _descriptor.FieldDescriptor(
      name='hourBeginning', full_name='activitiessniffer.Date.hourBeginning', index=2,
      number=3, type=9, cpp_type=9, label=1,
      has_default_value=False, default_value=b"".decode('utf-8'),
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR,  create_key=_descriptor._internal_create_key),
    _descriptor.FieldDescriptor(
      name='hourEnd', full_name='activitiessniffer.Date.hourEnd', index=3,
      number=4, type=9, cpp_type=9, label=1,
      has_default_value=False, default_value=b"".decode('utf-8'),
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR,  create_key=_descriptor._internal_create_key),
  ],
  extensions=[
  ],
  nested_types=[],
  enum_types=[
  ],
  serialized_options=None,
  is_extendable=False,
  syntax='proto3',
  extension_ranges=[],
  oneofs=[
  ],
  serialized_start=55,
  serialized_end=141,
)


_DIFFICULTY = _descriptor.Descriptor(
  name='Difficulty',
  full_name='activitiessniffer.Difficulty',
  filename=None,
  file=DESCRIPTOR,
  containing_type=None,
  create_key=_descriptor._internal_create_key,
  fields=[
    _descriptor.FieldDescriptor(
      name='Physical', full_name='activitiessniffer.Difficulty.Physical', index=0,
      number=1, type=5, cpp_type=1, label=1,
      has_default_value=False, default_value=0,
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR,  create_key=_descriptor._internal_create_key),
    _descriptor.FieldDescriptor(
      name='Technical', full_name='activitiessniffer.Difficulty.Technical', index=1,
      number=2, type=5, cpp_type=1, label=1,
      has_default_value=False, default_value=0,
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR,  create_key=_descriptor._internal_create_key),
    _descriptor.FieldDescriptor(
      name='Elevation', full_name='activitiessniffer.Difficulty.Elevation', index=2,
      number=3, type=5, cpp_type=1, label=1,
      has_default_value=False, default_value=0,
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR,  create_key=_descriptor._internal_create_key),
  ],
  extensions=[
  ],
  nested_types=[],
  enum_types=[
  ],
  serialized_options=None,
  is_extendable=False,
  syntax='proto3',
  extension_ranges=[],
  oneofs=[
  ],
  serialized_start=143,
  serialized_end=211,
)


_REGISTRATION = _descriptor.Descriptor(
  name='Registration',
  full_name='activitiessniffer.Registration',
  filename=None,
  file=DESCRIPTOR,
  containing_type=None,
  create_key=_descriptor._internal_create_key,
  fields=[
    _descriptor.FieldDescriptor(
      name='State', full_name='activitiessniffer.Registration.State', index=0,
      number=1, type=9, cpp_type=9, label=1,
      has_default_value=False, default_value=b"".decode('utf-8'),
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR,  create_key=_descriptor._internal_create_key),
    _descriptor.FieldDescriptor(
      name='NbMax', full_name='activitiessniffer.Registration.NbMax', index=1,
      number=2, type=5, cpp_type=1, label=1,
      has_default_value=False, default_value=0,
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR,  create_key=_descriptor._internal_create_key),
    _descriptor.FieldDescriptor(
      name='NbOk', full_name='activitiessniffer.Registration.NbOk', index=2,
      number=3, type=5, cpp_type=1, label=1,
      has_default_value=False, default_value=0,
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR,  create_key=_descriptor._internal_create_key),
    _descriptor.FieldDescriptor(
      name='NbWaitingList', full_name='activitiessniffer.Registration.NbWaitingList', index=3,
      number=4, type=5, cpp_type=1, label=1,
      has_default_value=False, default_value=0,
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR,  create_key=_descriptor._internal_create_key),
  ],
  extensions=[
  ],
  nested_types=[],
  enum_types=[
  ],
  serialized_options=None,
  is_extendable=False,
  syntax='proto3',
  extension_ranges=[],
  oneofs=[
  ],
  serialized_start=213,
  serialized_end=294,
)


_DESCRIPTION = _descriptor.Descriptor(
  name='Description',
  full_name='activitiessniffer.Description',
  filename=None,
  file=DESCRIPTOR,
  containing_type=None,
  create_key=_descriptor._internal_create_key,
  fields=[
    _descriptor.FieldDescriptor(
      name='General', full_name='activitiessniffer.Description.General', index=0,
      number=1, type=9, cpp_type=9, label=1,
      has_default_value=False, default_value=b"".decode('utf-8'),
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR,  create_key=_descriptor._internal_create_key),
    _descriptor.FieldDescriptor(
      name='Level', full_name='activitiessniffer.Description.Level', index=1,
      number=2, type=9, cpp_type=9, label=1,
      has_default_value=False, default_value=b"".decode('utf-8'),
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR,  create_key=_descriptor._internal_create_key),
    _descriptor.FieldDescriptor(
      name='Equipment', full_name='activitiessniffer.Description.Equipment', index=2,
      number=3, type=9, cpp_type=9, label=1,
      has_default_value=False, default_value=b"".decode('utf-8'),
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR,  create_key=_descriptor._internal_create_key),
  ],
  extensions=[
  ],
  nested_types=[],
  enum_types=[
  ],
  serialized_options=None,
  is_extendable=False,
  syntax='proto3',
  extension_ranges=[],
  oneofs=[
  ],
  serialized_start=296,
  serialized_end=360,
)


_ACTIVITY = _descriptor.Descriptor(
  name='Activity',
  full_name='activitiessniffer.Activity',
  filename=None,
  file=DESCRIPTOR,
  containing_type=None,
  create_key=_descriptor._internal_create_key,
  fields=[
    _descriptor.FieldDescriptor(
      name='ID', full_name='activitiessniffer.Activity.ID', index=0,
      number=1, type=9, cpp_type=9, label=1,
      has_default_value=False, default_value=b"".decode('utf-8'),
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR,  create_key=_descriptor._internal_create_key),
    _descriptor.FieldDescriptor(
      name='Title', full_name='activitiessniffer.Activity.Title', index=1,
      number=2, type=9, cpp_type=9, label=1,
      has_default_value=False, default_value=b"".decode('utf-8'),
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR,  create_key=_descriptor._internal_create_key),
    _descriptor.FieldDescriptor(
      name='DateInfo', full_name='activitiessniffer.Activity.DateInfo', index=2,
      number=3, type=11, cpp_type=10, label=1,
      has_default_value=False, default_value=None,
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR,  create_key=_descriptor._internal_create_key),
    _descriptor.FieldDescriptor(
      name='Place', full_name='activitiessniffer.Activity.Place', index=3,
      number=4, type=9, cpp_type=9, label=1,
      has_default_value=False, default_value=b"".decode('utf-8'),
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR,  create_key=_descriptor._internal_create_key),
    _descriptor.FieldDescriptor(
      name='DifficultyInfo', full_name='activitiessniffer.Activity.DifficultyInfo', index=4,
      number=5, type=11, cpp_type=10, label=1,
      has_default_value=False, default_value=None,
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR,  create_key=_descriptor._internal_create_key),
    _descriptor.FieldDescriptor(
      name='RegistrationInfo', full_name='activitiessniffer.Activity.RegistrationInfo', index=5,
      number=6, type=11, cpp_type=10, label=1,
      has_default_value=False, default_value=None,
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR,  create_key=_descriptor._internal_create_key),
    _descriptor.FieldDescriptor(
      name='Responsible', full_name='activitiessniffer.Activity.Responsible', index=6,
      number=7, type=9, cpp_type=9, label=1,
      has_default_value=False, default_value=b"".decode('utf-8'),
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR,  create_key=_descriptor._internal_create_key),
    _descriptor.FieldDescriptor(
      name='DescriptionInfo', full_name='activitiessniffer.Activity.DescriptionInfo', index=7,
      number=8, type=11, cpp_type=10, label=1,
      has_default_value=False, default_value=None,
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR,  create_key=_descriptor._internal_create_key),
    _descriptor.FieldDescriptor(
      name='Link', full_name='activitiessniffer.Activity.Link', index=8,
      number=9, type=9, cpp_type=9, label=1,
      has_default_value=False, default_value=b"".decode('utf-8'),
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR,  create_key=_descriptor._internal_create_key),
  ],
  extensions=[
  ],
  nested_types=[],
  enum_types=[
  ],
  serialized_options=None,
  is_extendable=False,
  syntax='proto3',
  extension_ranges=[],
  oneofs=[
  ],
  serialized_start=363,
  serialized_end=664,
)


_GETACTIVITIES_REQUEST = _descriptor.Descriptor(
  name='GetActivities_Request',
  full_name='activitiessniffer.GetActivities_Request',
  filename=None,
  file=DESCRIPTOR,
  containing_type=None,
  create_key=_descriptor._internal_create_key,
  fields=[
    _descriptor.FieldDescriptor(
      name='page_address', full_name='activitiessniffer.GetActivities_Request.page_address', index=0,
      number=1, type=9, cpp_type=9, label=1,
      has_default_value=False, default_value=b"".decode('utf-8'),
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR,  create_key=_descriptor._internal_create_key),
  ],
  extensions=[
  ],
  nested_types=[],
  enum_types=[
  ],
  serialized_options=None,
  is_extendable=False,
  syntax='proto3',
  extension_ranges=[],
  oneofs=[
  ],
  serialized_start=666,
  serialized_end=711,
)


_GETACTIVITIES_RESPONSE = _descriptor.Descriptor(
  name='GetActivities_Response',
  full_name='activitiessniffer.GetActivities_Response',
  filename=None,
  file=DESCRIPTOR,
  containing_type=None,
  create_key=_descriptor._internal_create_key,
  fields=[
    _descriptor.FieldDescriptor(
      name='Activities', full_name='activitiessniffer.GetActivities_Response.Activities', index=0,
      number=1, type=11, cpp_type=10, label=3,
      has_default_value=False, default_value=[],
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR,  create_key=_descriptor._internal_create_key),
  ],
  extensions=[
  ],
  nested_types=[],
  enum_types=[
  ],
  serialized_options=None,
  is_extendable=False,
  syntax='proto3',
  extension_ranges=[],
  oneofs=[
  ],
  serialized_start=713,
  serialized_end=786,
)

_ACTIVITY.fields_by_name['DateInfo'].message_type = _DATE
_ACTIVITY.fields_by_name['DifficultyInfo'].message_type = _DIFFICULTY
_ACTIVITY.fields_by_name['RegistrationInfo'].message_type = _REGISTRATION
_ACTIVITY.fields_by_name['DescriptionInfo'].message_type = _DESCRIPTION
_GETACTIVITIES_RESPONSE.fields_by_name['Activities'].message_type = _ACTIVITY
DESCRIPTOR.message_types_by_name['Date'] = _DATE
DESCRIPTOR.message_types_by_name['Difficulty'] = _DIFFICULTY
DESCRIPTOR.message_types_by_name['Registration'] = _REGISTRATION
DESCRIPTOR.message_types_by_name['Description'] = _DESCRIPTION
DESCRIPTOR.message_types_by_name['Activity'] = _ACTIVITY
DESCRIPTOR.message_types_by_name['GetActivities_Request'] = _GETACTIVITIES_REQUEST
DESCRIPTOR.message_types_by_name['GetActivities_Response'] = _GETACTIVITIES_RESPONSE
_sym_db.RegisterFileDescriptor(DESCRIPTOR)

Date = _reflection.GeneratedProtocolMessageType('Date', (_message.Message,), {
  'DESCRIPTOR' : _DATE,
  '__module__' : 'activities_sniffer_service_pb2'
  # @@protoc_insertion_point(class_scope:activitiessniffer.Date)
  })
_sym_db.RegisterMessage(Date)

Difficulty = _reflection.GeneratedProtocolMessageType('Difficulty', (_message.Message,), {
  'DESCRIPTOR' : _DIFFICULTY,
  '__module__' : 'activities_sniffer_service_pb2'
  # @@protoc_insertion_point(class_scope:activitiessniffer.Difficulty)
  })
_sym_db.RegisterMessage(Difficulty)

Registration = _reflection.GeneratedProtocolMessageType('Registration', (_message.Message,), {
  'DESCRIPTOR' : _REGISTRATION,
  '__module__' : 'activities_sniffer_service_pb2'
  # @@protoc_insertion_point(class_scope:activitiessniffer.Registration)
  })
_sym_db.RegisterMessage(Registration)

Description = _reflection.GeneratedProtocolMessageType('Description', (_message.Message,), {
  'DESCRIPTOR' : _DESCRIPTION,
  '__module__' : 'activities_sniffer_service_pb2'
  # @@protoc_insertion_point(class_scope:activitiessniffer.Description)
  })
_sym_db.RegisterMessage(Description)

Activity = _reflection.GeneratedProtocolMessageType('Activity', (_message.Message,), {
  'DESCRIPTOR' : _ACTIVITY,
  '__module__' : 'activities_sniffer_service_pb2'
  # @@protoc_insertion_point(class_scope:activitiessniffer.Activity)
  })
_sym_db.RegisterMessage(Activity)

GetActivities_Request = _reflection.GeneratedProtocolMessageType('GetActivities_Request', (_message.Message,), {
  'DESCRIPTOR' : _GETACTIVITIES_REQUEST,
  '__module__' : 'activities_sniffer_service_pb2'
  # @@protoc_insertion_point(class_scope:activitiessniffer.GetActivities_Request)
  })
_sym_db.RegisterMessage(GetActivities_Request)

GetActivities_Response = _reflection.GeneratedProtocolMessageType('GetActivities_Response', (_message.Message,), {
  'DESCRIPTOR' : _GETACTIVITIES_RESPONSE,
  '__module__' : 'activities_sniffer_service_pb2'
  # @@protoc_insertion_point(class_scope:activitiessniffer.GetActivities_Response)
  })
_sym_db.RegisterMessage(GetActivities_Response)



_ACTIVITIESSNIFFER_SERVICE = _descriptor.ServiceDescriptor(
  name='ActivitiesSniffer_Service',
  full_name='activitiessniffer.ActivitiesSniffer_Service',
  file=DESCRIPTOR,
  index=0,
  serialized_options=None,
  create_key=_descriptor._internal_create_key,
  serialized_start=789,
  serialized_end=918,
  methods=[
  _descriptor.MethodDescriptor(
    name='GetActivities',
    full_name='activitiessniffer.ActivitiesSniffer_Service.GetActivities',
    index=0,
    containing_service=None,
    input_type=_GETACTIVITIES_REQUEST,
    output_type=_GETACTIVITIES_RESPONSE,
    serialized_options=None,
    create_key=_descriptor._internal_create_key,
  ),
])
_sym_db.RegisterServiceDescriptor(_ACTIVITIESSNIFFER_SERVICE)

DESCRIPTOR.services_by_name['ActivitiesSniffer_Service'] = _ACTIVITIESSNIFFER_SERVICE

# @@protoc_insertion_point(module_scope)
